#Docker and ELK stack (including curator)


##Docker compose
- - -
```
docker-compose build

```
if you want to build the services first


```
docker-compose up -d --build
```
run in detached mode and start all services

```
docker-compose down
```
Stop and remove containers, networks, images, and volumes

###Scale es nodes
```
docker-compose scale node=3
```

##ElasticSearch
By default the image already installs the site plugin head.

You can access head  [here](http://localhost:9200/_plugin/head/)

If you want to install more, access the service and run
the cmd similar to below

Install plugin for service
```
docker-compose exec  elasticsearch /bin/bash
/usr/share/elasticsearch/bin/plugin install mobz/elasticsearch-head
```

##ElasticSearch Curator 4
By default the cron job is running once a minute. Access the service if you want to change the config or cron job

If you want you can mount a volume and then decide what file
you want to run

* access service
```
docker-compose exec  curator  /bin/sh
```
* tail logs
```
tail -f -n500 /curator/curator.log
```

* print env


```
docker-compose exec  curator  printenv
```

##FileBeats


```
filebeat -e -c filebeat.yml -d "publish"

docker-compose exec  beats /bin/bash
filebeat -e -v -c filebeat.yml
```


##Spring Boot App

App logs to file, console and to tcp to be picked up by logstash

```
./gradlew buildDocker

docker run --name app  -it
-p 8080:8080  shanelee007/jest-elastic
```


###Remove ALL Docker’s containers
```
$ docker rm $(docker ps -a -q)
```

###Remove ALL Untagged docker images
```
docker rmi $(docker images -q --filter "dangling=true")
```