package com.example;

import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.google.common.base.Supplier;
import io.searchbox.client.JestClient;
import io.searchbox.client.JestClientFactory;
import io.searchbox.client.config.HttpClientConfig;
import io.searchbox.indices.CreateIndex;
import jdk.nashorn.internal.objects.annotations.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.nio.client.HttpAsyncClientBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;
import vc.inreach.aws.request.AWSSigner;
import vc.inreach.aws.request.AWSSigningRequestInterceptor;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

import static java.lang.System.out;

@SpringBootApplication
@lombok.Setter
@Slf4j
@EnableScheduling
public class DemoApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    private static final String SERVICE = "es";

    @Value("${es.url}")
    private String clusterUri;

    @Autowired
    private JestClient jestClient;

    @Bean
    public JestClient jestClient() {

        UriComponents uriComponents =
                UriComponentsBuilder.newInstance()
                        .scheme("http").host(clusterUri).port(9200)
                        .build();

        final JestClientFactory factory = new JestClientFactory();
        factory.setHttpClientConfig(new HttpClientConfig
                .Builder(uriComponents.toUriString())
                .multiThreaded(true)
                .build());
        return factory.getObject();

    }

//    @Bean
//    public JestClient jestClient() throws Exception {
//
//        final Supplier<LocalDateTime> clock = () -> LocalDateTime.now(ZoneOffset.UTC);
//
//        DefaultAWSCredentialsProviderChain defaultAWSCredentialsProviderChain = new DefaultAWSCredentialsProviderChain();
//        Region region = Region.getRegion(Regions.fromName("ap-southeast-2"));
//        final AWSSigner awsSigner = new AWSSigner(defaultAWSCredentialsProviderChain, region.getName(),
//                SERVICE, clock);
//
//        final AWSSigningRequestInterceptor requestInterceptor = new AWSSigningRequestInterceptor(awsSigner);
//        final JestClientFactory factory = new JestClientFactory() {
//            @Override
//            protected HttpClientBuilder configureHttpClient(HttpClientBuilder builder) {
//                builder.addInterceptorLast(requestInterceptor);
//                return builder;
//            }
//
//            @Override
//            protected HttpAsyncClientBuilder configureHttpClient(HttpAsyncClientBuilder builder) {
//                builder.addInterceptorLast(requestInterceptor);
//                return builder;
//            }
//        };
//        factory.setHttpClientConfig(new HttpClientConfig
//                .Builder("https://search-shanesdomain-z7g6hzek3bnvdmnbgvgrlrqp2y.ap-southeast-2.es.amazonaws.com")
//                .multiThreaded(true)
//                .build());
//        return factory.getObject();
//
//    }

    @Override
    public void run(String... args) throws Exception {

        log.info("Hello World!");


        String settings = "\"settings\" : {\n" +
                "        \"number_of_shards\" : 5,\n" +
                "        \"number_of_replicas\" : 1\n" +
                "    }\n";

        jestClient.execute(new CreateIndex.Builder("test").build());

    }
}
