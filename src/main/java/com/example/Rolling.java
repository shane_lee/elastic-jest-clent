package com.example;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class Rolling {


    @Scheduled(cron = "0 * * * * MON-FRI" )
    public  void roll() {
        log.info("Rolling!!!");
    }
}
